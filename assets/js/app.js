var _optionSilderARow = function (btnNextEl, btnPrevEl) {
  return {
    slidesPerView: 1,
    spaceBetween: 50,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
      nextEl: btnNextEl,
      prevEl: btnPrevEl,
    },
    breakpoints: {
      340: {
        slidesPerView: 1,
        slidesPerColumn: 3,
        slidesPerGroup: 1,
        spaceBetween: 30,
      },
      360: {
        slidesPerView: 1,
        slidesPerColumn: 3,
        slidesPerGroup: 1,
        spaceBetween: 30,
      },
      640: {
        slidesPerView: 1,
        slidesPerColumn: 3,
        slidesPerGroup: 1,
        spaceBetween: 30,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
    }
  }
}

var _optionSilderThreeColumn = function(btnNextEl, btnPrevEl) {
  return {
    slidesPerView: 3,
    spaceBetween: 30,
    slidesPerGroup: 3,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
      nextEl: btnNextEl,
      prevEl: btnPrevEl,
    },
    breakpoints: {
        0:{
          slidesPerView: 1,
          spaceBetween: 20,
        },
      360: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
    }
  
  }
}
var _optionSilderThreeColumns = function(btnNextEl, btnPrevEl) {
  return {
    slidesPerView: 1,
    spaceBetween: 30,
    slidesPerGroup: 3,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
      nextEl: btnNextEl,
      prevEl: btnPrevEl,
    },
    breakpoints: {
        0:{
          slidesPerView: 1,
          spaceBetween: 20,
        },
      360: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
    }
  }
}

new Swiper('#blog .swiper-view-life', _optionSilderThreeColumn('.swiper-next', '.swiper-prev'));
new Swiper('#blog .swiper-experience', _optionSilderThreeColumn('.swiper-next-experience', '.swiper-prev-experience'));
new Swiper('#library .swiper-view-life', _optionSilderThreeColumn('.swiper-next', '.swiper-prev'));
new Swiper('#library .swiper-experience', _optionSilderThreeColumn('.swiper-next-experience', '.swiper-prev-experience'));
new Swiper('#community .swiper-startup', _optionSilderThreeColumn('.swiper-next-startup', '.swiper-prev-startup'));
new Swiper('#community .swiper-businessman', _optionSilderThreeColumn('.swiper-next-businessman', '.swiper-prev-businessman'));
new Swiper('#community .swiper-enterprise', _optionSilderThreeColumn('.swiper-next-enterprise', '.swiper-prev-enterprise'));
new Swiper('#index .swiper-community', _optionSilderThreeColumn('.swiper-next-community', '.swiper-prev-community'));
new Swiper('#index .swiper-educations', _optionSilderThreeColumn('.swiper-next-educations', '.swiper-prev-educations'));
new Swiper('#index .swiper-blog-index', _optionSilderThreeColumn('.swiper-next-blog-index', '.swiper-prev-blog-index'));
new Swiper('#library-1 .swiper-video-study', _optionSilderThreeColumn('.swiper-next-video-study', '.swiper-prev-video-study'));
new Swiper('#library-1 .swiper-tools', _optionSilderThreeColumn('.swiper-next-tools', '.swiper-prev-tools'));




new Swiper('#blog .swiper-business', _optionSilderARow('.swiper-next-busi', '.swiper-prev-busi'));
new Swiper('#blog .swiper-invest', _optionSilderARow('.swiper-next-invest', '.swiper-prev-invest'));
new Swiper('#blog .swiper-finance', _optionSilderARow('.swiper-next-finance', '.swiper-prev-finance'));
new Swiper('#blog .swiper-cultural', _optionSilderARow('.swiper-next-cultural', '.swiper-prev-cultural'));
new Swiper('#blog .swiper-health', _optionSilderARow('.swiper-next-health', '.swiper-prev-health'));
new Swiper('#blog .swiper-start-business', _optionSilderARow('.swiper-next-start-business', '.swiper-prev-start-business'));
new Swiper('#blog .swiper-corporate-governance', _optionSilderARow('.swiper-next-corporate-governance', '.swiper-prev-corporate-governance'));
new Swiper('#blog .swiper-operate-business', _optionSilderARow('.swiper-next-operate-business', '.swiper-prev-operate-business'));
new Swiper('#blog .swiper-corporate-culture', _optionSilderARow('.swiper-next-corporate-culture', '.swiper-prev-corporate-culture'));
new Swiper('#blog .swiper-enterprise-Law', _optionSilderARow('.swiper-next-enterprise-Law', '.swiper-prev-enterprise-Law'));
new Swiper('#community .swiper-idea', _optionSilderARow('.swiper-next-idea', '.swiper-prev-idea'));
new Swiper('#community .swiper-call-capital', _optionSilderARow('.swiper-next-call-capital', '.swiper-prev-call-capital'));
new Swiper('#community .swiper-busi-face', _optionSilderARow('.swiper-next-busi-face', '.swiper-prev-busi-face'));
new Swiper('#community .swiper-successful', _optionSilderARow('.swiper-next-successful', '.swiper-prev-successful'));
new Swiper('#community .swiper-successful-story', _optionSilderARow('.swiper-next-successful-story', '.swiper-prev-successful-story'));
new Swiper('#community .swiper-business-face', _optionSilderARow('.swiper-next-business-face', '.swiper-prev-business-face'));
new Swiper('#community .swiper-business-news', _optionSilderARow('.swiper-next-business-news', '.swiper-prev-business-news'));
new Swiper('#community .swiper-converter', _optionSilderARow('.swiper-next-converter', '.swiper-prev-converter'));
new Swiper('#community .swiper-representative', _optionSilderARow('.swiper-next-representative', '.swiper-prev-representative'));
new Swiper('#education .swiper-news-blog', _optionSilderARow('.swiper-next-news-blog', '.swiper-prev-news-blog'));

new Swiper('#education .swiper-blog-management', _optionSilderThreeColumns('.swiper-next-blog-management', '.swiper-prev-blog-management'));
new Swiper('#education .swiper-vocational-guidance', _optionSilderThreeColumns('.swiper-next-vocational-guidance', '.swiper-prev-vocational-guidance'));
new Swiper('#index .swiper-news', _optionSilderARow('.swiper-next-news', '.swiper-prev-news'));
new Swiper('#library-1 .swiper-ebook', _optionSilderARow('.swiper-next-ebook', '.swiper-prev-ebook'));
new Swiper('#library-1 .swiper-self-growth', _optionSilderARow('.swiper-next-self-growth', '.swiper-prev-self-growth'));
new Swiper('#library-1 .swiper-business', _optionSilderARow('.swiper-next-business', '.swiper-prev-business'));
new Swiper('#library-1 .swiper-document', _optionSilderARow('.swiper-next-document', '.swiper-prev-document'));
new Swiper('#page-content .swiper-related-posts', _optionSilderARow('.swiper-next-related-posts', '.swiper-prev-related-posts'))


new Swiper('#index .slide-header', {
  slidesPerView: 1,
  spaceBetween: 0,
  loop: true,
 
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

$('.btn-next').on('click', function () {
  $(this).find('i').css('color', '#616161');
  $(this).prev().find('i').css('color', '#B9B9B9');
})

$('.btn-prev').on('click', function () {
  $(this).find('i').css('color', '#616161');
  $(this).next().find('i').css('color', '#B9B9B9');
})


////////frame facebook
$(document).ready(function() {
	$('.popup').magnificPopup({
    type: 'iframe'
  });
});